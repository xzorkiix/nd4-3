const EventEmitter = require('events');

class ChatApp extends EventEmitter {
  /**
   * @param {String} title
   */
  constructor(title) {
    super();

    this.title = title;

    // Посылать каждую секунду сообщение
    setInterval(() => {
      this.emit('message', `${this.title}: ping-pong`);
    }, 1000);
  }
  // Часть 2.1
  close() {
    this.emit('close');
  }
}

let webinarChat =  new ChatApp('webinar');
let facebookChat = new ChatApp('=========facebook');
let vkChat =       new ChatApp('---------vk');

let chatOnMessage = (message) => {
  console.log(message);
};

webinarChat.on('message', chatOnMessage);
facebookChat.on('message', chatOnMessage);
vkChat.on('message', chatOnMessage);

// Часть 1.1
let chatOnMessagePrepaire = (message) => {
    console.log('Готовлюсь к ответу');
};

webinarChat.on('message', chatOnMessagePrepaire);

// Часть 1.2
vkChat.setMaxListeners(2);

// Часть 1.3
vkChat.on('message', chatOnMessagePrepaire);

// Часть 2.2
vkChat.on('close', () => {
    console.log('Чат вконтакте закрылся :(');
});
// Часть 2.3
vkChat.close();

// Закрыть вконтакте
setTimeout( ()=> {
  console.log('Закрываю вконтакте...');
  vkChat.removeListener('message', chatOnMessage);
}, 10000 );


// Закрыть фейсбук
setTimeout( ()=> {
  console.log('Закрываю фейсбук, все внимание — вебинару!');
  facebookChat.removeListener('message', chatOnMessage);
}, 15000 );

// Часть На зачет 1.
// Закрыть вебинар
setTimeout( ()=> {
  console.log('Закрываю фейсбук, все внимание — вебинару!');
  webinarChat.removeListener('message', chatOnMessage);
}, 30000 );
